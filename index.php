<form>
    <label for="inputNumber">Introduce un número:</label><br>
    <input type="number" id="inputNumber" name="inputNumber" value=""><br>
    <button type="submit" style="margin-top:1rem">Dibujar Piramide</button>
</form>

<?php

if (isset($_REQUEST['inputNumber'])) {
    $levels = 10;
    echo '<div style="margin:auto; text-align:center;">';
    for ($i = 0; $i < $levels; $i++) {
        for ($j = -$i; $j <= $i; $j++) {
            echo abs($_REQUEST['inputNumber'] * $j) + abs($_REQUEST['inputNumber']);
        }
        echo '<br>';
    }
    echo '</div>';
}
