# Script de prueba

El script permite al usuario crear una piramide de múltiplos de un número a partir del número en cuestion.

## Ejecución

Existen dos alternativas de ejecución del script:

### Ejecutar en servidor web
Coloque el archivo index.php en alguna ruta pública de su servidor web, abra el navegador y dirijase a la url correspondiente de la ruta seleccionada.
Ejemplo de ruta: 

```bash
/public_html/folder/
```
Url: http://su-dominio.com/folder/index.php (Si su servidor lo permite, puede omitir el index.php);


### Ejecutar en ambiente de desarrollo.
Coloque el archivo index.php en alguna carpeta pública de su entorno de desarrollo, abra el navegador y dirijase a la url correspondiente de la ruta seleccionada.
Ejemplo de ruta :

```bash
C:\xampp\htdocs\folder\
```
Url: http://localhost/folder/index.php (Si la configuración de su entorno de desarrollo lo permite, puede omitir el index.php);
